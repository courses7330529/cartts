import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'

import { CartProvider } from './context/CartProvider.tsx'
import { ProductsProvier } from './context/ProductsProvider.tsx'
ReactDOM.createRoot(document.getElementById('root')!).render(
  <ProductsProvier>
    <CartProvider>
      < App />
    </CartProvider>
  </ProductsProvier>
)
